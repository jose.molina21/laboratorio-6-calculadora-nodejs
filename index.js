//Importar clase express
import express from 'express';

//Crear un objeto express

const app = express();
const puerto = 3002;

//Crear rutas

app.get("/", (req, res) => {
    res.send("Bienvenido a la calculadora!!!");
});

//Suma
app.get("/sumar", (req, res) => {
    let resultadoS = Number(req.query.a) + Number(req.query.b);
    res.send(resultadoS.toString());
});

//Resta
app.get("/restar", (req, res) => {
    let resultadoR = Number(req.query.a) - Number(req.query.b);
    res.send(resultadoR.toString());
});

//Multiplicar
app.get("/multiplicar", (req, res) => {
    let resultadoM = Number(req.query.a) * Number(req.query.b);
    res.send(resultadoM.toString());
});

//Dividir
app.get("/dividir", (req, res) => {
    let resultadoD;
    let a1 = Number(req.query.a);
    let b1 = Number(req.query.b);
    if (b1 == 0) {
        resultadoD = "No se puede realizar la operacion";
    } else {
        resultadoD = a1 / b1;
    }
    res.send(resultadoD.toString());
});

//Modulo
app.get("/modulo", (req, res) => {
    let resultadoMo;
    let a1 = Number(req.query.a);
    let b1 = Number(req.query.b);
    if (b1 == 0) {
        resultadoMo = "No se puede realizar la operacion";
    } else {
        resultadoMo = a1 % b1;
    }

    res.send(resultadoMo.toString());
});


//Inicializar servidor

app.listen(puerto);